<?php

require_once __DIR__ . '/../../../Templates/Html/Header.php';
require_once __DIR__ . '/../../../Templates/Html/Nav.php';
?>

<div class="container mt-5 mb-5">
    <H1>Singleton</H1>
    <p>Все примеры взяты из книги Мэта Зандстры.</p>
    <p></p>
    <h4>Пример паттерна Singleton</h4>
    <p>Kласс Preferences - синглтон. Его не возможно создать через конструктор
        или клонировать. Он всегдо будет в единственном экземпляре.Который можно
        создать или получить только через статичекий метод. А после этого
        добавлять свойства через обычные гетеры и сметеры.</p>
    <pre><code data-language="php">
    class Preferences
    {

        private array $props = [];

        private static Preferences $instance;

        private function __construct()
        {
        }

        public static function getInstance(): Preferences
        {
            if (empty(self::$instance)) {
                self::$instance = new Preferences();
            }
            return self::$instance;
        }

        public function getProperty(string $key): key
        {
            return $this->props[$key];
        }

        public function setProperty(string $key, string $val): void
        {
            $this->props[$key] = $val;
        }

        private function __clone()
        {
        }

        public function __wakeup()
        {
            throw new Exception("Cannot unserialize singleton");
        }
    }
    </code></pre>

    <p>Создаем класс Preferences. Добавляем свойство. Пробуем создать еще один
        такой же класс и проверяем есть ли в нем свойство которое мы добавили
        первому классу. </p>

    <pre><code data-language="php">
    $preference1 = Preferences::getInstance();
    $preference1->setProperty("test", "fest");

    $preference2 = Preferences::getInstance();
    $preference2->getProperty("test"); // => "fest"
    </code></pre>

    <?php
    require_once __DIR__ . '/../../../Templates/Html/Footer.php';
    ?>

