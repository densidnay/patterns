<?php

require_once __DIR__ . '/../../../Templates/Html/Header.php';
require_once __DIR__ . '/../../../Templates/Html/Nav.php';
?>

<div class="container mt-5 mb-5">
    <H1>Decorator</H1>
    <p>Все примеры взяты из книги Мэта Зандстры.</p>
    <p></p>
    <h4>Пример паттерна на примере игры похожей на цивилизацию.</h4>
    <p>класс Tile (клетка на поле игры) абстрактная клетка</p>
    <pre><code data-language="php">
    abstract class Tile
    {

        abstract public function getWealthFactor();

    }
    </code></pre>
    <p>класс Равнина наследуется от Tile, приносит доход $wealthFactor = 2 значение клетки равнина по умолчанию</p>
    <pre><code data-language="php">
    class Plains extends Tile
    {

        private int $wealthFactor = 2;

        public function getWealthFactor(): int
        {
            return $this->wealthFactor;
        }

    }
    </code></pre>
    <p>создаем декоратор, наследуемый от класса  Tile, и который принимает аргументом тип Tile</p>

    <pre><code data-language="php">
    abstract class TileDecorator extends Tile
    {

        protected Tile $tile;

        public function __construct(Tile $tile)
        {
            $this->tile = $tile;
        }

    }
    </code></pre>
    <p>наследуем от главного декоратора класс DiamondDecorator который изменяет доход клетки на + 2</p>

    <pre><code data-language="php">
    class DiamondDecorator extends TileDecorator
    {

        public function getWealthFactor() : int
        {
            return $this->tile->getWealthFactor() + 2;
        }

    }
    </code></pre>
    <p>наследуем от главного декоратора класс PollutionDecorator который изменяет доход клетки на - 4</p>

    <pre><code data-language="php">
    class PollutionDecorator extends TileDecorator
    {

        public function getWealthFactor(): int
        {
            return $this->tile->getWealthFactor() - 4;
        }

    }
    </code></pre>

    <p>В итоге мы можем создать класс равнина и получить доход => 2</p>

    <pre><code data-language="php">
    $tile = new Plains();
    print $tile->getWealthFactor(); // => 2
    </code></pre>
    <p>Или сделать Равнину с алмазами добавив обычной Равнине поведение клетки с алмазами => 2 + 2 = 4</p>

    <pre><code data-language="php">
    $tile = new DiamondDecorator(new Plains());
    print $tile->getWealthFactor(); // => 4
    </code></pre>
    <p>Или сделать Равнину с загрязнением добавив обычной Равнине поведение клетки с загрязнением (Pollution) => 2 - 4 = -2</p>

    <pre><code data-language="php">
    $tile = new PollutionDecorator(new Plains());
    print $tile->getWealthFactor(); // => -2
    </code></pre>
    <p>Или сделать Равнину с загрязнением и так же с алмазами добавив  Равнине поведение клетки с загрязнением и алмазами => 2 + 2 - 4 = 0</p>

    <pre><code data-language="php">
    $tile = new PollutionDecorator(new DiamondDecorator(new Plains()));
    print $tile->getWealthFactor(); // => 0
    </code></pre>

</div>
<?php
require_once __DIR__ . '/../../../Templates/Html/Footer.php';
?>

