<?php

class Preferences
{

    private array $props = [];

    private static Preferences $instance;

    private function __construct()
    {
    }

    public static function getInstance(): Preferences
    {
        if (empty(self::$instance)) {
            self::$instance = new Preferences();
        }
        return self::$instance;
    }

    public function getProperty(string $key): key
    {
        return $this->props[$key];
    }

    public function setProperty(string $key, string $val): void
    {
        $this->props[$key] = $val;
    }

    private function __clone()
    {
    }

    public function __wakeup()
    {
        throw new Exception("Cannot unserialize singleton");
    }
}