<?php

namespace Drums\Patterns\Patterns\Structural\Decorator\ZandstraSimple;

class Plains extends Tile
{

    private int $wealthFactor = 2;

    public function getWealthFactor(): int
    {
        return $this->wealthFactor;
    }

}