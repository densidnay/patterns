<?php

namespace Drums\Patterns\Patterns\Structural\Decorator\ZandstraSimple;

class PollutionDecorator extends TileDecorator
{

    public function getWealthFactor(): int
    {
        return $this->tile->getWealthFactor() - 4;
    }

}