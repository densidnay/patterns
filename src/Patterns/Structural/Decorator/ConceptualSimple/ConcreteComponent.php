<?php

namespace Drums\Patterns\Patterns\Structural\Decorator\ConceptualSimple;

/**
 * Конкретные Компоненты предоставляют реализации поведения по умолчанию. Может
 * быть несколько вариаций этих классов.
 */
class ConcreteComponent implements Component
{

    public function operation(): string
    {
        return "ConcreteComponent";
    }

}