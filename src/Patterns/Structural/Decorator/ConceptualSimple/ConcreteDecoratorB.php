<?php

namespace Drums\Patterns\Patterns\Structural\Decorator\ConceptualSimple;

/**
 * Декораторы могут выполнять своё поведение до или после вызова обёрнутого
 * объекта.
 */
class ConcreteDecoratorB extends Decorator
{

    public function operation(): string
    {
        return "ConcreteDecoratorB(" . parent::operation() . ")";
    }

}