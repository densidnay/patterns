<?php

namespace Drums\Patterns\Patterns\Structural\Decorator\ConceptualSimple;

/**
 * Базовый интерфейс Компонента определяет поведение, которое изменяется
 * декораторами.
 */
interface Component
{

    public function operation(): string;

}